function random(min, max) {
    return Math.floor((Math.random() * max) + min);
}
function getId(id) {
    return document.getElementById(id);
}
function getClass(id) {
    return document.getElementsByClassName(id);
}
var Player = {
    Upgrades: {
        mineAtOnce: 1
    },
    stone: 0,
    coal: 0,
    copper: 0
};
function update() {
    getId("stone").innerHTML = Player.stone.toString();
    getId("coal").innerHTML = Player.coal.toString();
    getId("copper").innerHTML = Player.copper.toString();
}
function mine(amount) {
    for (var x = 0; x < amount; x++) {
        console.log("mining");
        var ran = random(0, 100);
        if (ran < 10) {
            Player.copper++;
        }
        else if (ran < 30) {
            Player.coal++;
        }
        else {
            Player.stone++;
        }
    }
}
setInterval(function () {
    update();
    if (Player.stone > 0) {
        getClass("stone")[0].classList = "stone";
        getClass("stone")[1].classList = "stone";
    }
    if (Player.coal > 0) {
        getClass("coal")[0].classList = "coal";
        getClass("coal")[1].classList = "coal";
    }
    if (Player.copper > 0) {
        getClass("copper")[0].classList = "copper";
        getClass("copper")[1].classList = "copper";
    }
}, 20);
//# sourceMappingURL=main.js.map